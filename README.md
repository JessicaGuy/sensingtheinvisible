# SensingTheInvisible
The project “Sensing the invisible” is an invitation into a conversation about perception, data, and how we can create a balanced synergy between technology, nature and humans. It goes beyond data-visualisation and is an indirect approach to data representation which will help us to understand and make sense of information from data sets. The electronic organ which is part of collective nodes is being used to create a shared experience of data perception and distributes a mesh for decision making. Dwellers of the future cities will not be passive receivers of their environment, but active transceivers of understanding and reflecting.

There are four main components to make the electronic sensing organ. It is made out of an input, output, the fabduino or [Arduino Uno](https://store.arduino.cc/arduino-uno-rev3) which controls everything and the design structure. (Basic knowledge of coding, soldering and making in general is required! Or at least a passion for learning ;-) )

![](Images/Organ.jpg)

### BOM
- 1x Ardunio Uno or Fabduino / Detailed information for the Fabduino is following
- 1x Copper PCB
- 3x MOSFET IRF Z44N
- 3x LRA haptic driver motors
- 2x Resistor 10K
- 1x Resistor 47R
- 3x Capacitor 1nF = 1000 pF
- 1x Capacitor 100nf = 10000pF
- 1x LT5534
- 1x 4 AA Battery holder

- Extra Wires
- Shrinking cable
- Fab isp
- FTDI Cable

### Machines and Tools
- 3D printer
- Solder iron and Solder
- Small milling Machine + End mills
- Multimeter
- Heat gun or similar
- Pliers
- Cutting Pliers
- Tweezers
- Silicon solder mat (optional)
- Helping hand for soldering (optional)
  like [this one](https://www.amazon.com/ProsKit-900-015-Helping-Hands-Soldering/dp/B01E62773K) for example
- Arduino Software installed on your computer


LOADS OF PASSION AND PATIENCE !

![](Images/Tools.jpg)


## Input
The first component which has to be made is the input. The input is an electromagnetic frequency (EMF) detector which is inspired by [Lab 3 the Laboratory for Experimental Computer Science at the Academy of Media Arts](http://interface.khm.de/index.php/lab/interfaces-advanced/radio-signal-strength-sensor/) There are two options to make the PCB. Either with a milling machine on a hard PCB or with a vinyl cutter for the flexible version. I recommend to do the hard PCB but I have added the files for the flexible one as well.

1. At first the the PCB for the EMF detector has to be milled. The files for the milling can be found [here](https://gitlab.com/JessicaGuy/sensingtheinvisible/tree/master/Electronic_Files/EMF_Detector_Input). Feel free to play around with the schematic in [Eagle](https://www.autodesk.com/products/eagle/overview) if you want! Upload the files to [Fabmodules](http://fabmodules.org/) and then open the file with the software of your milling machine. Set your X,Y and Z of your machine on the copper board! First you will have to cut the 1/64 traces files and then the 1/32 holes / outcut file.

For detailed information on how to use fabmodules, exporting the file and cutting it on the milling machine follow [these instructions](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/mods.html).

2. The next step is to solder all the components to the PCB. For this I would recommend to have the schematic open to place the components to the right places. Make sure that you check the direction of the LT5534 microchip. It will have a small indication which will tell you where the VCC is. A magnifying glass may be helpful for this, as the microchip is unfortunately tiny. Another tip I can give is to make sure not to stay to long on one place with the solder-iron. It will fry your components!

3. Now its time to hook up your EMF detector to your Arduino! In case you have decided to make your own Fabduino follow the instructions below. Add this code to a new file of your Arduino. Compile the sketch and then open your serial monitor. Feel free to use any output for the EMF detector! You could also use LED's or sound, basically its time to play now! If you also want to make a wearable like me go to the next "OUPUT" step!

 ```c#
int analogIn = 0;
int analogValue = 0;
int pinLed = 13;
int cnt;

void setup(){
  Serial.begin(57600);
  pinMode(pinLed,OUTPUT);

}

void loop(){

  analogValue=0;
  for (cnt=0;cnt< 100;cnt++) {
    digitalWrite(pinLed,1);
    analogIn=analogRead(1);
    digitalWrite(pinLed,0);
    if (analogIn > analogValue) analogValue=analogIn;
    delayMicroseconds(100);

  }
  Serial.println(analogValue);
  delay(100);

}
```

For vinyl cutting the PCB you can follow [this](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/flexible_pcb_windows_mac.html) tutorial from FabAcademy.


## Output
The output in my case are three Linear Resonant Actuators or LRA's motors in combination with Metal Oxide Semiconductor Field Effect Transistor or short MOSFET. The MOSFETs are transistors which work as a semiconductor device. It works as a switch for energy or amplifies electronic signals in electronic devices.

1. In my case the first step was to use the Arduino Uno board and a breadboard plus some dupont wires to connect the MOSFETS and the LRA's to the Arduino. This helped me to check if the connections and the code is working. But in this case you can directly jump into soldering everything if you want.

2. (Some background information) The MOSFET's have a **Gain**, **Drain**, and **Source**. The gain is connected to the PWM pins of the Arduino Uno. Gain is an amplitude of a signal from the input to the output. The drain is connected to the negative pole of the LRA. The source is connected to the negative of the battery and the ground of the Arduino Uno. For more information on MOSFET's I can recommend this [page](https://www.electronics-tutorials.ws/transistor/tran_6.html)

![](/Images/Schematic.jpg)

3. Let's go soldering! Soldering is one of my favourite parts of electronics. Follow the schematic below to solder all the parts together.

![](/Images/Soldering_Output.jpg)

## Fabduino or Arduino

1. The next step is to connect everything to your Arduino. And add the code to a new file. Make sure that you are using the same **Pin Numbers** for connecting your EMF detector + vibration motors and in your code.

2. If you have decided to make your own Fabduino then it is time so mill the next board now! You can find all files in the [Fabduino folder](https://gitlab.com/JessicaGuy/sensingtheinvisible/tree/master/Electronic_Files/EMF_Detector_Input) in this Repo. This procedure is the same just as the EMF detector. You will have to upload your file to Fabmodules, and then set up the X,Y and Z of your machine and then launch the file. After milling you will have to solder all the components.

3. Next step is programming it! For this you will need the Fab isp and the FTDI cable. Check out this [documentation](http://academy.kaziunas.com/tutorials/hello_arduino.php) for detailed information.

And in case that is not functioning with the first try you will have to troubleshoot it. If you are not able to find the problem (checking with your multimeter etc.) I would advice you to google it. There are many forums online which a very helpful, or feel free to contact me!

## 3D Printing

I have added the files for the 3D printing in this Repo. I have printed my wearable/ organ with PLA filaflex as it is a more sustainable material. Also the filaflex is as the name says flexible and will work well on the body. Safe the file as an STL and then upload it to your 3D printer, and go!

Another possibility is to make a flexible PCB which uses adhesive copper tape. For that I will recommend you to follow [this](http://fabacademy.org/2019/docs/FabAcademy-Tutorials/week04_electronic_production/flexible_pcb_windows_mac.html) tutorial.

As soon as the 3D print is finished it is time to assemble everything. Feel free to play around with the inputs and outputs! And if you have any feedback for me please message me!

Have fun!
